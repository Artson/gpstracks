﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace GPSTracks.Tests
{
    public class CoordinateTests
    {
        [Theory]
        [InlineData(47.714488, -117.411492, 47.719925, -117.266425, 10.87, 2)]
        [InlineData(10.734622, 18.253118, -39.173883, 175.928899, 16157.67, 0)]
        public void KilometersTo_NormalValues_CorrectResult(double latitudeA, double longitudeA, double latitudeB, double longitudeB, double distanceInKm, int precision)
        {
            Coordinate a = new Coordinate() { Latitude = latitudeA, Longitude = longitudeA };
            Coordinate b = new Coordinate() { Latitude = latitudeB, Longitude = longitudeB };

            Assert.Equal(distanceInKm, a.KilometersTo(b), precision);
        }

        [Theory]
        [InlineData(47.714488, -117.411492, 47.719925, -117.266425, 6.75, 2)]
        [InlineData(10.734622, 18.253118, -39.173883, 175.928899, 10039.91, 0)]
        public void MilesTo_NormalValues_CorrectResult(double latitudeA, double longitudeA, double latitudeB, double longitudeB, double distanceInMiles, int precision)
        {
            Coordinate a = new Coordinate() { Latitude = latitudeA, Longitude = longitudeA };
            Coordinate b = new Coordinate() { Latitude = latitudeB, Longitude = longitudeB };

            Assert.Equal(distanceInMiles, a.MilesTo(b), precision);
        }

        [Theory]
        [InlineData(90, 0, 0, 0, 45, 0)]
        [InlineData(0, 90, 0, 0, 0, 45)]
        public void Midpoint_NormalValues_CorrectResult(double latitudeA, double longitudeA, double latitudeB, double longitudeB, double midpointLatitude, double midpointLongitude)
        {
            Coordinate a = new Coordinate() { Latitude = latitudeA, Longitude = longitudeA };
            Coordinate b = new Coordinate() { Latitude = latitudeB, Longitude = longitudeB };

            Coordinate midpoint = a.Midpoint(b);

            Assert.Equal(midpointLatitude, midpoint.Latitude);
            Assert.Equal(midpointLongitude, midpoint.Longitude);
        }
    }
}
