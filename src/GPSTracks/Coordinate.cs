﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GPSTracks
{
    public class Coordinate
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        public DateTime TimeStamp { get; set; }

        public double MilesTo(Coordinate other)
        {
            return KilometersTo(other) * 0.621371;
        }

        //haversine formula
        public double KilometersTo(Coordinate other)
        {
            var R = 6371.0008; // metres
            var latARad = DegreesToRadians(Latitude);
            var latBRad = DegreesToRadians(other.Latitude);
            var deltaLatRad = DegreesToRadians(Latitude - other.Latitude);
            var deltaLongRad = DegreesToRadians(Longitude - other.Longitude);

            var a = Math.Sin(deltaLatRad / 2) * Math.Sin(deltaLatRad / 2) +
                    Math.Cos(latARad) * Math.Cos(latBRad) *
                    Math.Sin(deltaLongRad / 2) * Math.Sin(deltaLongRad / 2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            var d = R * c;

            return d;
        }

        public Coordinate Midpoint(Coordinate other)
        {
            Coordinate midpoint = new Coordinate();

            double dLon = DegreesToRadians(other.Longitude - Longitude);
            double Bx = Math.Cos(DegreesToRadians(other.Latitude)) * Math.Cos(dLon);
            double By = Math.Cos(DegreesToRadians(other.Latitude)) * Math.Sin(dLon);

            midpoint.Latitude = RadiansToDegrees(Math.Atan2(
                         Math.Sin(DegreesToRadians(Latitude)) + Math.Sin(DegreesToRadians(other.Latitude)),
                         Math.Sqrt(
                             (Math.Cos(DegreesToRadians(Latitude)) + Bx) *
                             (Math.Cos(DegreesToRadians(Latitude)) + Bx) + By * By)));

            midpoint.Longitude = Longitude + RadiansToDegrees(Math.Atan2(By, Math.Cos(DegreesToRadians(Latitude)) + Bx));

            return midpoint;
        }

        private double DegreesToRadians(double degrees)
        {
            return Math.PI * degrees / 180;
        }

        private double RadiansToDegrees(double radians)
        {
            return radians * 180 / Math.PI;
        }
    }
}
