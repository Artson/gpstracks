﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GPSTracks
{
    public class Track
    {
        public IList<Coordinate> Points { get; private set; }
        
        public Track()
        {
            Points = new List<Coordinate>();
        }
    }
}
